package com.jalauni.apicis.middleware;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${urlApiLegacyAuthDecode}")
    private String decodeUrl;

    private ResponseEntity<Map<String, String>> getUserDataFromApi(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(token);

        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        try {
            ResponseEntity<Map<String, String>> responseEntity = restTemplate.exchange(decodeUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<Map<String, String>>() {});
            return responseEntity;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        if (requestURI.contains("/swagger") || requestURI.contains("/v3/api-docs")) {
            return true;
        }

        String authorizationHeader = request.getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String token = authorizationHeader.substring(7);

            ResponseEntity<Map<String, String>> responseEntity = getUserDataFromApi(token);

            if (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.OK) {
                request.setAttribute("userData", responseEntity.getBody());
                return true;
            }
        }
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        return false;
    }
}
