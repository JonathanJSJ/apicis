package com.jalauni.apicis.utils.factory;

import com.jalauni.apicis.model.dtos.TopicRequestDto;
import com.jalauni.apicis.model.dtos.TopicResponseDto;
import com.jalauni.apicis.model.entities.Topic;

public class TopicFactory {
    public static Topic buildTopic(TopicRequestDto topicRequestDto, String userId) {
        Topic topic = new Topic();
        topic.setTitle(topicRequestDto.title());
        topic.setDescription(topicRequestDto.description());
        topic.setUserId(userId);
        return topic;
    }

    public static TopicResponseDto buildTopicResponseDto(Topic topic) {
        return new TopicResponseDto(topic.getId().toHexString(), topic.getTitle(), topic.getDescription());
    }
}
