package com.jalauni.apicis.model.repositories;

import com.jalauni.apicis.model.entities.Topic;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicsRepository extends MongoRepository<Topic, ObjectId> {
}
