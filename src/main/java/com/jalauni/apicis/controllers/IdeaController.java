package com.jalauni.apicis.controllers;

import com.jalauni.apicis.model.dtos.IdeaRequestDto;
import com.jalauni.apicis.model.dtos.IdeaResponseDto;
import com.jalauni.apicis.model.services.IdeasService;
import com.jalauni.apicis.utils.exception.EntityNotFoundException;
import com.jalauni.apicis.utils.exception.PersistenceException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/ideas")
public class IdeaController {

    private final IdeasService ideasService;

    @Autowired
    public IdeaController(IdeasService ideasService) {
        this.ideasService = ideasService;
    }

    @Operation(description = "Creates a new idea for a specific topic.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Returns the new idea."),
            @ApiResponse(responseCode = "404", description = "The topic with the specified id doesn't exist.")
    })
    @PostMapping("/topic/{topicId}")
    public ResponseEntity<IdeaResponseDto> postIdea(HttpServletRequest request, @PathVariable ObjectId topicId, @RequestBody @Valid IdeaRequestDto ideaDto) throws PersistenceException, EntityNotFoundException {
        String userId = ((Map<String, String>) request.getAttribute("userData")).get("id");

        IdeaResponseDto responseDto = ideasService.createIdea(ideaDto, userId, topicId);
        return ResponseEntity.status(HttpStatus.CREATED).body(responseDto);
    }

    @Operation(description = "Gets ideas of a specific topic.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns a page of ideas."),
            @ApiResponse(responseCode = "404", description = "The topic with the specified id doesn't exist.")
    })
    @GetMapping("topic/{topicId}")
    public ResponseEntity<Page<IdeaResponseDto>> getAllIdeasByTopic(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size, @PathVariable ObjectId topicId) throws EntityNotFoundException {
        Pageable pageable = PageRequest.of(page, size);
        Page<IdeaResponseDto> ideas = ideasService.getAllIdeasByTopic(pageable, topicId);
        return ResponseEntity.ok(ideas);
    }

    @Operation(description = "Gets a idea by it ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns the idea."),
            @ApiResponse(responseCode = "404", description = "The idea with the specified id doesn't exist.")
    })
    @GetMapping("{id}")
    public ResponseEntity<IdeaResponseDto> getIdeaById(@PathVariable ObjectId id) throws EntityNotFoundException {
        IdeaResponseDto responseDto = ideasService.getIdeaById(id);
        return ResponseEntity.ok(responseDto);

    }

    @Operation(description = "Updates a idea by it ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns the updated idea."),
            @ApiResponse(responseCode = "404", description = "The idea with the specified id doesn't exist.")
    })
    @PutMapping("/{id}")
    public ResponseEntity<IdeaResponseDto> updateIdea(@PathVariable ObjectId id, @RequestBody @Valid IdeaRequestDto ideaDto) throws EntityNotFoundException {
        IdeaResponseDto responseDto = ideasService.updateIdea(id, ideaDto);
        return ResponseEntity.ok(responseDto);

    }

    @Operation(description = "Deletes a idea by it ID.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Idea deleted."),
            @ApiResponse(responseCode = "404", description = "The idea with the specified id doesn't exist.")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteIdea(@PathVariable ObjectId id) throws EntityNotFoundException {
        ideasService.deleteIdea(id);
        return ResponseEntity.noContent().build();
    }

}
