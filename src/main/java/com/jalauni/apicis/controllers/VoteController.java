package com.jalauni.apicis.controllers;

import com.jalauni.apicis.model.dtos.VoteRequestDto;
import com.jalauni.apicis.model.dtos.VoteResponseDto;
import com.jalauni.apicis.model.services.VotesService;
import com.jalauni.apicis.utils.exception.EntityNotFoundException;
import com.jalauni.apicis.utils.exception.PersistenceException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.apache.coyote.BadRequestException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/votes")
public class VoteController {

    private final VotesService votesService;

    @Autowired
    public VoteController(VotesService votesService) {
        this.votesService = votesService;
    }

    @Operation(description = "Creates a new vote for a specific idea.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Returns the new idea."),
            @ApiResponse(responseCode = "400", description = "The user has already voted."),
            @ApiResponse(responseCode = "404", description = "The idea with the specified id doesn't exist.")
    })
    @PostMapping("/idea/{ideaId}")
    public ResponseEntity<VoteResponseDto> postVote(HttpServletRequest request, @PathVariable ObjectId ideaId, @RequestBody @Valid VoteRequestDto voteRequestDto) throws BadRequestException, EntityNotFoundException, PersistenceException {
        String userId = ((Map<String, String>) request.getAttribute("userData")).get("id");

        VoteResponseDto voteDto = votesService.createVote(voteRequestDto, userId, ideaId);
        return ResponseEntity.status(HttpStatus.CREATED).body(voteDto);
    }

    @Operation(description = "Gets votes of a specific idea.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns a page of votes."),
            @ApiResponse(responseCode = "404", description = "The idea with the specified id doesn't exist.")
    })
    @GetMapping("/idea/{ideaId}")
    public ResponseEntity<Page<VoteResponseDto>> getVotesByIdea(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size, @PathVariable ObjectId ideaId) throws EntityNotFoundException {
        Pageable pageable = PageRequest.of(page, size);
        Page<VoteResponseDto> votes = votesService.getVotesByIdea(pageable, ideaId);
        return ResponseEntity.ok(votes);
    }

    @Operation(description = "Gets a specific vote.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns the vote."),
            @ApiResponse(responseCode = "404", description = "The vote with the specified id doesn't exist.")
    })
    @GetMapping("/{voteId}")
    public ResponseEntity<VoteResponseDto> getVote(@PathVariable ObjectId voteId) throws EntityNotFoundException {
        VoteResponseDto response = votesService.getVoteById(voteId);
        return ResponseEntity.ok(response);
    }

    @Operation(description = "Updates a specific vote.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns the updated vote."),
            @ApiResponse(responseCode = "400", description = "The status of the vote is the same as the request."),
            @ApiResponse(responseCode = "404", description = "The vote with the specified id doesn't exist.")
    })
    @PutMapping("/{voteId}")
    public ResponseEntity<VoteResponseDto> putVote(@PathVariable ObjectId voteId, @RequestBody @Valid VoteRequestDto voteRequestDto) throws BadRequestException, EntityNotFoundException {
        VoteResponseDto response = votesService.updateVote(voteId, voteRequestDto);
        return ResponseEntity.ok(response);
    }

    @Operation(description = "Deletes a specific vote.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Vote deleted."),
            @ApiResponse(responseCode = "404", description = "The vote with the specified id doesn't exist.")
    })
    @DeleteMapping("/{voteId}")
    public ResponseEntity<VoteResponseDto> deleteVote(@PathVariable ObjectId voteId) throws EntityNotFoundException {
        votesService.deleteVote(voteId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
