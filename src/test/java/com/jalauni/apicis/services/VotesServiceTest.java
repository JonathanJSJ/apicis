package com.jalauni.apicis.services;

import com.jalauni.apicis.model.dtos.VoteRequestDto;
import com.jalauni.apicis.model.dtos.VoteResponseDto;
import com.jalauni.apicis.model.entities.Idea;
import com.jalauni.apicis.model.entities.Vote;
import com.jalauni.apicis.model.repositories.IdeaRepository;
import com.jalauni.apicis.model.repositories.VotesRepository;
import com.jalauni.apicis.model.services.VotesService;
import com.jalauni.apicis.utils.exception.EntityNotFoundException;
import com.jalauni.apicis.utils.exception.PersistenceException;
import lombok.SneakyThrows;
import org.apache.coyote.BadRequestException;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class VotesServiceTest {

    @MockBean
    private VotesRepository votesRepository;

    @MockBean
    private IdeaRepository ideaRepository;

    @InjectMocks
    private VotesService votesService;

    private VoteRequestDto requestDto;
    private String userId;
    private Idea idea;
    private Vote vote;

    @BeforeEach
    void setUp() {
        requestDto = new VoteRequestDto(true);
        userId = UUID.randomUUID().toString();

        idea = new Idea();
        idea.setId(new ObjectId());

        vote = new Vote();
        vote.setId(new ObjectId());
        vote.setPositiveVote(requestDto.isPositiveVote());
        vote.setUserId(userId);
        vote.setIdea(idea);
    }

    @Test
    void createVote_IdeaNotFound_ThrowsException() {
        when(ideaRepository.findById(any())).thenReturn(Optional.empty());

        try {
            votesService.createVote(requestDto, userId, idea.getId());
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(EntityNotFoundException.class, e.getClass()),
                    () -> assertEquals("Idea not found.", e.getMessage())
            );
        }

        verify(ideaRepository, times(1)).findById(any());
    }

    @Test
    void createVote_VoteAlreadyExists_ThrowsException() {
        when(ideaRepository.findById(idea.getId())).thenReturn(Optional.of(idea));
        when(votesRepository.findVoteByIdeaIdAndUserId(any(), any())).thenReturn(Optional.of(new Vote()));

        try {
            votesService.createVote(requestDto, userId, idea.getId());
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(BadRequestException.class, e.getClass()),
                    () -> assertEquals("Vote already exists.", e.getMessage())
            );
        }

        verify(votesRepository, times(1)).findVoteByIdeaIdAndUserId(any(), any());
    }

    @Test
    void createVote_VoteNotSaved_ThrowsException() {
        when(ideaRepository.findById(idea.getId())).thenReturn(Optional.of(idea));
        when(votesRepository.findVoteByIdeaIdAndUserId(any(), any())).thenReturn(Optional.empty());
        when(votesRepository.findById(any())).thenReturn(Optional.empty());

        try {
            votesService.createVote(requestDto, userId, idea.getId());
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(PersistenceException.class, e.getClass()),
                    () -> assertEquals("Vote not saved.", e.getMessage())
            );
        }

        verify(votesRepository, times(1)).findById(any());
    }

    @SneakyThrows
    @Test
    void createVote_PositiveVote() {
        Idea idea = new Idea();
        VoteRequestDto voteRequestDto = new VoteRequestDto(true);

        when(ideaRepository.findById(idea.getId())).thenReturn(Optional.of(idea));
        when(votesRepository.findVoteByIdeaIdAndUserId(any(), any())).thenReturn(Optional.empty());
        when(votesRepository.findById(any())).thenReturn(Optional.of(vote));

        VoteResponseDto responseDto = votesService.createVote(voteRequestDto, userId, idea.getId());

        Assertions.assertAll(
                () -> assertNotNull(responseDto),
                () -> assertEquals(1, idea.getPositiveVotes())
        );

        verify(votesRepository, times(1)).findById(any());
    }

    @SneakyThrows
    @Test
    void createVote_NegativeVote() {
        Idea idea = new Idea();
        VoteRequestDto voteRequestDto = new VoteRequestDto(false);

        when(ideaRepository.findById(idea.getId())).thenReturn(Optional.of(idea));
        when(votesRepository.findVoteByIdeaIdAndUserId(any(), any())).thenReturn(Optional.empty());
        when(votesRepository.findById(any())).thenReturn(Optional.of(vote));

        VoteResponseDto responseDto = votesService.createVote(voteRequestDto, userId, idea.getId());

        Assertions.assertAll(
                () -> assertNotNull(responseDto),
                () -> assertEquals(1, idea.getNegativeVotes())
        );

        verify(votesRepository, times(1)).findById(any());
    }

    @Test
    void getVoteById_VoteNotFound_ThrowsException() {
        when(votesRepository.findById(any())).thenReturn(Optional.empty());

        try {
            votesService.getVoteById(vote.getId());
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(EntityNotFoundException.class, e.getClass()),
                    () -> assertEquals("Vote not found.", e.getMessage())
            );
        }

        verify(votesRepository, times(1)).findById(any());
    }

    @SneakyThrows
    @Test
    void getVoteById() {
        when(votesRepository.findById(vote.getId())).thenReturn(Optional.of(vote));

        VoteResponseDto responseDto = votesService.getVoteById(vote.getId());

        assertAll(
                () -> assertNotNull(responseDto),
                () -> assertEquals(vote.getId().toHexString(), responseDto.voteId()),
                () -> assertEquals(vote.isPositiveVote(), responseDto.isPositiveVote())
        );

        verify(votesRepository, times(1)).findById(any());
    }

    @Test
    void getVotesByIdea_IdeaNotFound_ThrowsException() {
        when(ideaRepository.findById(any())).thenReturn(Optional.empty());

        try {
            votesService.getVotesByIdea(PageRequest.of(0, 5), idea.getId());
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(EntityNotFoundException.class, e.getClass()),
                    () -> assertEquals("Idea not found.", e.getMessage())
            );
        }
    }

    @SneakyThrows
    @Test
    void getVoteByIdea() {
        Pageable pageable = PageRequest.of(0, 5);
        when(ideaRepository.findById(idea.getId())).thenReturn(Optional.of(idea));
        when(votesRepository.findVoteByIdeaId(any(), any())).thenReturn(new PageImpl<>(new ArrayList<>(), pageable, 0));

        Page<VoteResponseDto> responseDtoPage = votesService.getVotesByIdea(pageable, idea.getId());

        assertNotNull(responseDtoPage);
        verify(votesRepository, times(1)).findVoteByIdeaId(any(), any());
    }

    @Test
    void updateVote_VoteNotFound_ThrowsException() {
        when(votesRepository.findById(vote.getId())).thenReturn(Optional.empty());

        try {
            votesService.updateVote(vote.getId(), new VoteRequestDto(true));
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(EntityNotFoundException.class, e.getClass()),
                    () -> assertEquals("Vote not found.", e.getMessage())
            );
        }

        verify(votesRepository, times(1)).findById(any());
    }

    @Test
    void updateVote_IdeaNotFound_ThrowsException() {
        when(votesRepository.findById(vote.getId())).thenReturn(Optional.of(vote));
        when(ideaRepository.findById(any())).thenReturn(Optional.empty());

        try {
            votesService.updateVote(vote.getId(), new VoteRequestDto(true));
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(EntityNotFoundException.class, e.getClass()),
                    () -> assertEquals("Idea not found.", e.getMessage())
            );
        }

        verify(ideaRepository, times(1)).findById(any());
    }

    @Test
    void updateVote_RequestEqualsVoteState(){
        vote.setPositiveVote(true);

        when(votesRepository.findById(vote.getId())).thenReturn(Optional.of(vote));
        when(ideaRepository.findById(any())).thenReturn(Optional.of(idea));

        try {
            votesService.updateVote(vote.getId(), new VoteRequestDto(true));
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(BadRequestException.class, e.getClass()),
                    () -> assertEquals("Vote is already in this state.", e.getMessage())
            );
        }
    }

    @SneakyThrows
    @Test
    void updateVote_ChangeToPositiveVote(){
        idea.setNegativeVotes(1);
        idea.setPositiveVotes(0);
        vote.setPositiveVote(false);

        when(votesRepository.findById(vote.getId())).thenReturn(Optional.of(vote));
        when(ideaRepository.findById(any())).thenReturn(Optional.of(idea));

        votesService.updateVote(vote.getId(), new VoteRequestDto(true));

        assertAll(
                ()->  assertTrue(vote.isPositiveVote()),
                ()->  assertEquals(1, idea.getPositiveVotes()),
                ()->  assertEquals(0, idea.getNegativeVotes())
        );
    }

    @SneakyThrows
    @Test
    void updateVote_ChangeToNegativeVote(){
        idea.setNegativeVotes(0);
        idea.setPositiveVotes(1);
        vote.setPositiveVote(true);

        when(votesRepository.findById(vote.getId())).thenReturn(Optional.of(vote));
        when(ideaRepository.findById(any())).thenReturn(Optional.of(idea));

        votesService.updateVote(vote.getId(), new VoteRequestDto(false));

        assertAll(
                () -> assertFalse(vote.isPositiveVote()),
                () -> assertEquals(0, idea.getPositiveVotes()),
                () -> assertEquals(1, idea.getNegativeVotes())
        );
    }

    @Test
    void deleteVote_VoteNotFound_ThrowsException() {
        when(votesRepository.findById(any())).thenReturn(Optional.empty());

        try {
            votesService.deleteVote(vote.getId());
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(EntityNotFoundException.class, e.getClass()),
                    () -> assertEquals("Vote not found.", e.getMessage())
            );
        }

        verify(votesRepository, times(1)).findById(any());
    }

    @Test
    void deleteVote_IdeaNotFound_ThrowsException() {
        when(votesRepository.findById(vote.getId())).thenReturn(Optional.of(vote));
        when(ideaRepository.findById(any())).thenReturn(Optional.empty());

        try {
            votesService.deleteVote(vote.getId());
            fail("Should have thrown an exception");
        } catch (Exception e) {
            assertAll(
                    () -> assertEquals(EntityNotFoundException.class, e.getClass()),
                    () -> assertEquals("Idea not found.", e.getMessage())
            );
        }

        verify(ideaRepository, times(1)).findById(any());
    }

    @SneakyThrows
    @Test
    void deleteVote_PositiveVote() {
        vote.setPositiveVote(true);
        idea.setPositiveVotes(1);

        when(votesRepository.findById(vote.getId())).thenReturn(Optional.of(vote));
        when(ideaRepository.findById(any())).thenReturn(Optional.of(idea));

        votesService.deleteVote(vote.getId());

        verify(votesRepository, times(1)).delete(vote);
        assertEquals(0, idea.getPositiveVotes());
    }

    @SneakyThrows
    @Test
    void deleteVote_NegativeVote() {
        vote.setPositiveVote(false);
        idea.setNegativeVotes(1);

        when(votesRepository.findById(vote.getId())).thenReturn(Optional.of(vote));
        when(ideaRepository.findById(any())).thenReturn(Optional.of(idea));

        votesService.deleteVote(vote.getId());

        verify(votesRepository, times(1)).delete(vote);
        assertEquals(0, idea.getNegativeVotes());
    }
}