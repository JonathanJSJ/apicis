package com.jalauni.apicis.model.repositories;

import com.jalauni.apicis.model.entities.Vote;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface VotesRepository extends MongoRepository<Vote, ObjectId> {
    Page<Vote> findVoteByIdeaId(Pageable pageable, ObjectId ideaId);

    Optional<Vote> findVoteByIdeaIdAndUserId(ObjectId ideaId, String userId);

    //TEST
    Optional<Vote> findFirstByIdeaId(ObjectId ideaId);
}
