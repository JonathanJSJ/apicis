package com.jalauni.apicis.model.dtos;

public record TopicResponseDto(String topicId, String title, String description) {
}
