package com.jalauni.apicis.model.dtos;

import jakarta.validation.constraints.NotNull;

public record IdeaRequestDto (@NotNull String description){
}
