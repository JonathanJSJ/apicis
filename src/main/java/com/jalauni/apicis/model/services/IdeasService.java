package com.jalauni.apicis.model.services;

import com.jalauni.apicis.model.dtos.IdeaRequestDto;
import com.jalauni.apicis.model.dtos.IdeaResponseDto;
import com.jalauni.apicis.model.entities.Idea;
import com.jalauni.apicis.model.entities.Topic;
import com.jalauni.apicis.model.repositories.IdeaRepository;
import com.jalauni.apicis.model.repositories.TopicsRepository;
import com.jalauni.apicis.utils.exception.EntityNotFoundException;
import com.jalauni.apicis.utils.exception.PersistenceException;
import com.jalauni.apicis.utils.factory.IdeaFactory;
import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IdeasService {

    @Autowired
    private IdeaRepository ideasRepository;

    @Autowired
    private TopicsRepository topicsRepository;

    public IdeaResponseDto createIdea(IdeaRequestDto ideaDto, String userId, ObjectId topicId) throws EntityNotFoundException, PersistenceException {
        Optional<Topic> topic = topicsRepository.findById(topicId);
        if (topic.isEmpty()){
            throw new EntityNotFoundException("Topic not found.");
        }

        Idea idea = IdeaFactory.buildIdea(ideaDto, userId, topic.get());

        ideasRepository.save(idea);

        Optional<Idea> response = ideasRepository.findById(idea.getId());

        if (response.isEmpty()){
            throw new PersistenceException("Idea not saved.");
        }
        return IdeaFactory.buildIdeaResponseDto(response.get());
    }

    public Page<IdeaResponseDto> getAllIdeasByTopic(Pageable pageable, ObjectId topicId) throws EntityNotFoundException {
        Optional<Topic> topic = topicsRepository.findById(topicId);
        if (topic.isEmpty()){
            throw new EntityNotFoundException("Topic not found.");
        }

        Page<Idea> ideasPage = ideasRepository.findIdeasByTopic(pageable, topic.get());
        List<IdeaResponseDto> ideaResponseDtoList = ideasPage.getContent().stream()
                .map(IdeaFactory::buildIdeaResponseDto)
                .toList();

        return new PageImpl<>(ideaResponseDtoList, pageable, ideasPage.getTotalElements());
    }

    public IdeaResponseDto getIdeaById(ObjectId ideaId) throws EntityNotFoundException {
        Optional<Idea> idea = ideasRepository.findById(ideaId);

        if (idea.isEmpty()){
            throw new EntityNotFoundException("Idea not found.");
        }

        return IdeaFactory.buildIdeaResponseDto(idea.get());
    }

    public IdeaResponseDto updateIdea(ObjectId ideaId, IdeaRequestDto ideaDto) throws EntityNotFoundException {
        Optional<Idea> ideaOptional = ideasRepository.findById(ideaId);

        if (ideaOptional.isEmpty()) {
            throw new EntityNotFoundException("Idea not found.");
        }

        Idea updatedIdea = ideaOptional.get();

        BeanUtils.copyProperties(ideaDto, updatedIdea);

        ideasRepository.save(updatedIdea);

        return IdeaFactory.buildIdeaResponseDto(updatedIdea);
    }

    public void deleteIdea(ObjectId ideaId) throws EntityNotFoundException {
        Optional<Idea> idea = ideasRepository.findById(ideaId);

        if (idea.isEmpty()) {
            throw new EntityNotFoundException("Idea not found.");
        }

        ideasRepository.delete(idea.get());
    }

}
