package com.jalauni.apicis.model.dtos;

import jakarta.validation.constraints.NotNull;

public record VoteRequestDto (@NotNull boolean isPositiveVote){
}
