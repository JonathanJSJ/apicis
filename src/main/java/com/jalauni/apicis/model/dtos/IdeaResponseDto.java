package com.jalauni.apicis.model.dtos;

public record IdeaResponseDto (String id, String description, int positiveVotes, int negativeVotes){
}
