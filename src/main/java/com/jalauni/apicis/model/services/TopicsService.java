package com.jalauni.apicis.model.services;

import com.jalauni.apicis.model.dtos.TopicRequestDto;
import com.jalauni.apicis.model.dtos.TopicResponseDto;
import com.jalauni.apicis.model.entities.Topic;
import com.jalauni.apicis.model.repositories.TopicsRepository;
import com.jalauni.apicis.utils.exception.EntityNotFoundException;
import com.jalauni.apicis.utils.exception.PersistenceException;
import com.jalauni.apicis.utils.factory.TopicFactory;
import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class TopicsService {

    @Autowired
    private TopicsRepository topicsRepository;

    public TopicResponseDto createTopic(TopicRequestDto dto, String userId) throws PersistenceException {
        Topic topic = TopicFactory.buildTopic(dto, userId);

        topicsRepository.save(topic);

        Optional<Topic> topicOptional = topicsRepository.findById(topic.getId());

        if (topicOptional.isEmpty()) {
            throw new PersistenceException("Topic not saved");
        }
        return TopicFactory.buildTopicResponseDto(topicOptional.get());
    }

    public Page<TopicResponseDto> getAllTopics(Pageable pageable){
        Page<Topic> topicsPage =  topicsRepository.findAll(pageable);
        List<TopicResponseDto> topicsDtoList = topicsPage.getContent()
                .stream()
                .map(TopicFactory::buildTopicResponseDto)
                .toList();

        return new PageImpl<>(topicsDtoList, topicsPage.getPageable(), topicsPage.getTotalElements());
    }

    public TopicResponseDto getTopic(ObjectId id) throws EntityNotFoundException {
        Optional<Topic> response = topicsRepository.findById(id);

        if (response.isEmpty()){
            throw new EntityNotFoundException("Topic not found.");
        }

        return TopicFactory.buildTopicResponseDto(response.get());
    }

    public TopicResponseDto updateTopic(ObjectId id, TopicRequestDto topicDto) throws EntityNotFoundException {
        Optional<Topic> topicOptional = topicsRepository.findById(id);

        if (topicOptional.isEmpty()) {
            throw new EntityNotFoundException("Topic not found.");
        }

        Topic updatedTopic = topicOptional.get();

        BeanUtils.copyProperties(topicDto, updatedTopic,
                Arrays.asList("id", "creationDate").toArray(String[]::new));

        topicsRepository.save(updatedTopic);

        return TopicFactory.buildTopicResponseDto(updatedTopic);
    }

    public void deleteTopic(ObjectId id) throws EntityNotFoundException {
        Optional<Topic> topicOptional = topicsRepository.findById(id);

        if (topicOptional.isEmpty()) {
            throw new EntityNotFoundException("Topic not found.");
        }

        topicsRepository.delete(topicOptional.get());
    }

}
