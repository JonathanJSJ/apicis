package com.jalauni.apicis.model.entities;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Data
@Document(collection = "votes")
public class Vote {
    @MongoId
    private ObjectId id;

    private String userId;

    private Idea idea;

    private boolean positiveVote;
}
