package com.jalauni.apicis.services;

import com.jalauni.apicis.model.dtos.TopicRequestDto;
import com.jalauni.apicis.model.entities.Topic;
import com.jalauni.apicis.model.repositories.TopicsRepository;
import com.jalauni.apicis.model.services.TopicsService;
import lombok.SneakyThrows;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class TopicsServiceTest {

    @MockBean
    private TopicsRepository topicsRepository;

    @InjectMocks
    private TopicsService topicsService;

    private Topic topic;

    @BeforeEach
    void setUp() {
        topic = new Topic();
        topic.setId(new ObjectId());
        topic.setUserId(UUID.randomUUID().toString());
        topic.setTitle("John Doe");
        topic.setDescription("This is a test");
    }

    @SneakyThrows
    @Test
    void createTopic() {
        when(topicsRepository.findById(any())).thenReturn(Optional.of(topic));
        var topicServiceResponse = topicsService.createTopic(new TopicRequestDto(topic.getTitle(), topic.getDescription()), UUID.randomUUID().toString());

        assertAll(
                () -> assertNotNull(topicServiceResponse),
                () -> assertEquals(topicServiceResponse.topicId(), topic.getId().toString()),
                () -> assertEquals(topicServiceResponse.title(), topic.getTitle()),
                () -> assertEquals(topicServiceResponse.description(), topic.getDescription())
        );
    }

    @Test
    void getAllTopics() {
        var topicList = List.of(topic, topic, topic, topic, topic);
        var topicPage = new PageImpl<>(topicList);
        when(topicsRepository.findAll(any(Pageable.class))).thenReturn(topicPage);

        var topicPageResponse = topicsService.getAllTopics(Pageable.unpaged());

        assertAll(
                () -> assertNotNull(topicPageResponse),
                () -> assertEquals(topicList.size(), topicPageResponse.getContent().size())
        );

        verify(topicsRepository).findAll(any(Pageable.class));
    }

    @SneakyThrows
    @Test
    void getTopic() {
        when(topicsRepository.findById(topic.getId())).thenReturn(Optional.of(topic));

        var topicResponse = topicsService.getTopic(topic.getId());

        assertAll(
                () -> assertNotNull(topicResponse),
                () -> assertEquals(topicResponse.topicId(), topic.getId().toString()),
                () -> assertEquals(topicResponse.description(), topic.getDescription()),
                () -> assertEquals(topicResponse.title(), topic.getTitle())
        );
    }

    @SneakyThrows
    @Test
    void updateTopic() {
        when(topicsRepository.findById(topic.getId())).thenReturn(Optional.of(topic));

        var topicRequest = new TopicRequestDto("Other one", "Other");

        var topicResponse = topicsService.updateTopic(topic.getId(), topicRequest);

        assertAll(
                () -> assertNotNull(topicResponse),
                () -> assertEquals(topicResponse.title(), topicRequest.title()),
                () -> assertEquals(topicResponse.description(), topicRequest.description())
        );

    }

    @Test
    void deleteTopic() {
        when(topicsRepository.findById(topic.getId())).thenReturn(Optional.of(topic));

        assertDoesNotThrow(() -> topicsService.deleteTopic(topic.getId()));

        verify(topicsRepository).delete(topic);
    }
}