package com.jalauni.apicis.utils.exception;

public class PersistenceException extends Exception{
    public PersistenceException(String message) {
        super(message);
    }
}
