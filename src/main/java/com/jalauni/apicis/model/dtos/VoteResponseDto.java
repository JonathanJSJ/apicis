package com.jalauni.apicis.model.dtos;

public record VoteResponseDto (String voteId, boolean isPositiveVote){
}
