package com.jalauni.apicis.model.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

@Data
@Document(collection = "ideas")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Idea {
    @MongoId
    private ObjectId id;

    private String userId;

    private Topic topic;

    private String description;

    private int positiveVotes = 0;

    private int negativeVotes = 0;

    @DBRef
    private List<Vote> votes;
}
