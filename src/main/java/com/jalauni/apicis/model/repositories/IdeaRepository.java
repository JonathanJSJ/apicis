package com.jalauni.apicis.model.repositories;

import com.jalauni.apicis.model.entities.Idea;
import com.jalauni.apicis.model.entities.Topic;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IdeaRepository extends MongoRepository<Idea, ObjectId> {
    Page<Idea> findIdeasByTopic(Pageable pageable, Topic topic);
}
