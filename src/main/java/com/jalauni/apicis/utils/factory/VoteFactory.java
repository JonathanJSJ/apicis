package com.jalauni.apicis.utils.factory;

import com.jalauni.apicis.model.dtos.VoteRequestDto;
import com.jalauni.apicis.model.dtos.VoteResponseDto;
import com.jalauni.apicis.model.entities.Idea;
import com.jalauni.apicis.model.entities.Vote;

public class VoteFactory {

    public static Vote buildVote(VoteRequestDto voteDto, String userId, Idea idea){
        Vote vote = new Vote();
        vote.setUserId(userId);
        vote.setIdea(idea);
        vote.setPositiveVote(voteDto.isPositiveVote());

        return vote;
    }

    public static VoteResponseDto buildVoteResponseDto(Vote vote){
        return new VoteResponseDto(vote.getId().toHexString(), vote.isPositiveVote());
    }
}
