package com.jalauni.apicis.model.entities;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

@Data
@Document(collection = "topics")
public class Topic {
    @MongoId
    private ObjectId id;

    private String userId;

    private String title;

    private String description;

    @DBRef
    private List<Idea> ideas;
}
