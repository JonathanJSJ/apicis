package com.jalauni.apicis.model.dtos;

import jakarta.validation.constraints.NotBlank;

public record TopicRequestDto(@NotBlank String title, @NotBlank String description) {
}
