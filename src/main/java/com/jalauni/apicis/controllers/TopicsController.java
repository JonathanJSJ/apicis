package com.jalauni.apicis.controllers;

import com.jalauni.apicis.model.dtos.TopicRequestDto;
import com.jalauni.apicis.model.dtos.TopicResponseDto;
import com.jalauni.apicis.model.services.TopicsService;
import com.jalauni.apicis.utils.exception.EntityNotFoundException;
import com.jalauni.apicis.utils.exception.PersistenceException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("api/topics")
public class TopicsController {

    private final TopicsService topicsService;

    @Autowired
    public TopicsController(TopicsService topicsService) {
        this.topicsService = topicsService;
    }

    @Operation(description = "Creates a topic.")
    @ApiResponse(responseCode = "201", description = "Returns the new topic.")
    @PostMapping
    public ResponseEntity<TopicResponseDto> postTopic(HttpServletRequest request, @RequestBody @Valid TopicRequestDto topicRequest) throws PersistenceException {
        Map<String, String> userData = (Map<String, String>) request.getAttribute("userData");

        TopicResponseDto topicResponse = topicsService.createTopic(topicRequest, userData.get("id"));
        return ResponseEntity.status(HttpStatus.CREATED).body(topicResponse);
    }

    @Operation(description = "Gets all topics.")
    @ApiResponse(responseCode = "200", description = "Returns a page of topics.")
    @GetMapping
    public ResponseEntity<Page<TopicResponseDto>> getAllTopics(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<TopicResponseDto> topics = topicsService.getAllTopics(pageable);
        return ResponseEntity.ok(topics);
    }

    @Operation(description = "Gets a specific topic.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns the topic."),
            @ApiResponse(responseCode = "404", description = "The topic with the specified id doesn't exist.")
    })
    @GetMapping("/{id}")
    public ResponseEntity<TopicResponseDto> getTopic(@PathVariable ObjectId id) throws EntityNotFoundException {
        TopicResponseDto responseDto = topicsService.getTopic(id);
        return ResponseEntity.ok(responseDto);
    }

    @Operation(description = "Updates a specific topic.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns the updated idea."),
            @ApiResponse(responseCode = "404", description = "The topic with the specified id doesn't exist.")
    })
    @PutMapping("/{id}")
    public ResponseEntity<TopicResponseDto> updateTopic(@PathVariable ObjectId id, @RequestBody @Valid TopicRequestDto topicDto) throws EntityNotFoundException {
        TopicResponseDto responseDto = topicsService.updateTopic(id, topicDto);
        return ResponseEntity.ok(responseDto);
    }

    @Operation(description = "Deletes a specific topic.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Topic deleted."),
            @ApiResponse(responseCode = "404", description = "The topic with the specified id doesn't exist.")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTopic(@PathVariable ObjectId id) throws EntityNotFoundException {
        topicsService.deleteTopic(id);
        return ResponseEntity.noContent().build();
    }

}
