package com.jalauni.apicis.utils.factory;

import com.jalauni.apicis.model.dtos.IdeaRequestDto;
import com.jalauni.apicis.model.dtos.IdeaResponseDto;
import com.jalauni.apicis.model.entities.Idea;
import com.jalauni.apicis.model.entities.Topic;

public class IdeaFactory {
    public static Idea buildIdea(IdeaRequestDto ideaDto, String userId, Topic topic){
        Idea idea = new Idea();
        idea.setDescription(ideaDto.description());
        idea.setUserId(userId);
        idea.setTopic(topic);

        return idea;
    }

    public static IdeaResponseDto buildIdeaResponseDto(Idea idea){
        return new IdeaResponseDto(idea.getId().toHexString(), idea.getDescription(), idea.getPositiveVotes(), idea.getNegativeVotes());
    }
}
