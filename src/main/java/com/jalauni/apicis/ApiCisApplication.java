package com.jalauni.apicis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCisApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCisApplication.class, args);
	}

}
