package com.jalauni.apicis.model.services;

import com.jalauni.apicis.model.dtos.VoteRequestDto;
import com.jalauni.apicis.model.dtos.VoteResponseDto;
import com.jalauni.apicis.model.entities.Idea;
import com.jalauni.apicis.model.entities.Vote;
import com.jalauni.apicis.model.repositories.IdeaRepository;
import com.jalauni.apicis.model.repositories.VotesRepository;
import com.jalauni.apicis.utils.exception.EntityNotFoundException;
import com.jalauni.apicis.utils.exception.PersistenceException;
import com.jalauni.apicis.utils.factory.VoteFactory;
import org.apache.coyote.BadRequestException;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VotesService {

    @Autowired
    private VotesRepository votesRepository;

    @Autowired
    private IdeaRepository ideaRepository;


    public VoteResponseDto createVote(VoteRequestDto voteRequestDto, String userId, ObjectId ideaId) throws BadRequestException, EntityNotFoundException, PersistenceException {
        Optional<Idea> idea = ideaRepository.findById(ideaId);
        if (idea.isEmpty()){
            throw new EntityNotFoundException("Idea not found.");
        }

        Optional<Vote> previewVote = votesRepository.findVoteByIdeaIdAndUserId(ideaId, userId);
        if (previewVote.isPresent()){
            throw new BadRequestException("Vote already exists.");
        }

        Vote vote = VoteFactory.buildVote(voteRequestDto, userId, idea.get());

        votesRepository.save(vote);

        Optional<Vote> response = votesRepository.findById(vote.getId());
        if (response.isEmpty()){
            throw new PersistenceException("Vote not saved.");
        }

        if (vote.isPositiveVote()){
            idea.get().setPositiveVotes(idea.get().getPositiveVotes() + 1);
        }else {
            idea.get().setNegativeVotes(idea.get().getNegativeVotes() + 1);
        }
        ideaRepository.save(idea.get());

        return VoteFactory.buildVoteResponseDto(response.get());
    }

    public VoteResponseDto getVoteById(ObjectId voteId) throws EntityNotFoundException {
        Optional<Vote> vote = votesRepository.findById(voteId);

        if (vote.isEmpty()){
            throw new EntityNotFoundException("Vote not found.");
        }

        return VoteFactory.buildVoteResponseDto(vote.get());
    }

    public Page<VoteResponseDto> getVotesByIdea(Pageable pageable, ObjectId ideaId) throws EntityNotFoundException {
        Optional<Idea> idea = ideaRepository.findById(ideaId);
        if (idea.isEmpty()){
            throw new EntityNotFoundException("Idea not found.");
        }

        Page<Vote> votePage = votesRepository.findVoteByIdeaId(pageable, idea.get().getId());
        List<VoteResponseDto> voteResponseDtoList = votePage.getContent()
                .stream()
                .map(VoteFactory::buildVoteResponseDto)
                .toList();

        return new PageImpl<>(voteResponseDtoList, pageable, votePage.getTotalElements());
    }

    public VoteResponseDto updateVote(ObjectId voteId, VoteRequestDto voteDto) throws EntityNotFoundException, BadRequestException {
        Optional<Vote> voteOptional = votesRepository.findById(voteId);
        if (voteOptional.isEmpty()){
            throw new EntityNotFoundException("Vote not found.");
        }
        Vote vote = voteOptional.get();

        Optional<Idea> ideaOptional = ideaRepository.findById(vote.getIdea().getId());
        if (ideaOptional.isEmpty()){
            throw new EntityNotFoundException("Idea not found.");
        }
        Idea idea = ideaOptional.get();

        if (vote.isPositiveVote() == voteDto.isPositiveVote()){
            throw new BadRequestException("Vote is already in this state.");
        }

        vote.setPositiveVote(voteDto.isPositiveVote());

        votesRepository.save(vote);

        if (vote.isPositiveVote()){
            idea.setPositiveVotes(idea.getPositiveVotes() + 1);
            idea.setNegativeVotes(idea.getNegativeVotes() - 1);
        }else {
            idea.setNegativeVotes(idea.getNegativeVotes() + 1);
            idea.setPositiveVotes(idea.getPositiveVotes() - 1);
        }
        ideaRepository.save(idea);

        return VoteFactory.buildVoteResponseDto(vote);
    }

    public void deleteVote(ObjectId voteId) throws EntityNotFoundException {
        Optional<Vote> vote = votesRepository.findById(voteId);
        if (vote.isEmpty()){
            throw new EntityNotFoundException("Vote not found.");
        }

        Optional<Idea> ideaOptional = ideaRepository.findById(vote.get().getIdea().getId());
        if (ideaOptional.isEmpty()){
            throw new EntityNotFoundException("Idea not found.");
        }
        Idea idea = ideaOptional.get();

        if (vote.get().isPositiveVote()){
            idea.setPositiveVotes(idea.getPositiveVotes() - 1);
        }else {
            idea.setNegativeVotes(idea.getNegativeVotes() - 1);
        }
        ideaRepository.save(idea);

        votesRepository.delete(vote.get());
    }
}
