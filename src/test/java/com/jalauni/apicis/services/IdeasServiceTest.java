package com.jalauni.apicis.services;

import com.jalauni.apicis.model.dtos.IdeaRequestDto;
import com.jalauni.apicis.model.dtos.IdeaResponseDto;
import com.jalauni.apicis.model.entities.Idea;
import com.jalauni.apicis.model.entities.Topic;
import com.jalauni.apicis.model.repositories.IdeaRepository;
import com.jalauni.apicis.model.repositories.TopicsRepository;
import com.jalauni.apicis.model.services.IdeasService;
import lombok.SneakyThrows;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class IdeasServiceTest {
    @MockBean
    private IdeaRepository ideasRepository;

    @MockBean
    private TopicsRepository topicsRepository;

    @InjectMocks
    private IdeasService ideasService;

    private Idea idea;
    private Topic topic;

    @BeforeEach
    void setUp() {
        topic = new Topic();
        topic.setTitle("John Doe");
        topic.setDescription("John Doe is the Best one");
        topic.setId(ObjectId.get());
        topic.setUserId(UUID.randomUUID().toString());

        idea = new Idea();
        idea.setId(ObjectId.get());
        idea.setPositiveVotes(50);
        idea.setNegativeVotes(340);
        idea.setUserId(UUID.randomUUID().toString());
        idea.setTopic(topic);
        idea.setDescription("description");
    }

    @SneakyThrows
    @Test
    void createIdea() {
        when(topicsRepository.findById(any(ObjectId.class))).thenReturn(Optional.of(topic));
        when(ideasRepository.findById(any())).thenReturn(Optional.of(idea));

        var ideaRequest = new IdeaRequestDto(idea.getDescription());

        var ideaResponse = ideasService.createIdea(ideaRequest, idea.getUserId(), topic.getId());

        assertAll(
                () -> assertNotNull(ideaResponse),
                () -> assertEquals(ideaResponse.description(), idea.getDescription()),
                () -> assertEquals(ideaResponse.positiveVotes(), idea.getPositiveVotes()),
                () -> assertEquals(ideaResponse.negativeVotes(), idea.getNegativeVotes()),
                () -> assertEquals(ideaResponse.id(), idea.getId().toString())
        );
    }

    @SneakyThrows
    @Test
    void getAllIdeasByTopic() {
        Pageable pageable = Pageable.unpaged();
        Page<Idea> ideasPage = new PageImpl<>(Collections.singletonList(idea));

        when(topicsRepository.findById(any())).thenReturn(Optional.of(topic));
        when(ideasRepository.findIdeasByTopic(any(), any())).thenReturn(ideasPage);

        Page<IdeaResponseDto> resultPage = ideasService.getAllIdeasByTopic(pageable, topic.getId());

        assertAll(
                () -> assertNotNull(resultPage),
                () -> assertEquals(1, resultPage.getContent().size())
        );

        var ideaResponse = resultPage.getContent().getFirst();

        assertAll(
                () -> assertEquals(idea.getDescription(), ideaResponse.description()),
                () -> assertEquals(idea.getPositiveVotes(), ideaResponse.positiveVotes()),
                () -> assertEquals(idea.getNegativeVotes(), ideaResponse.negativeVotes()),
                () -> assertEquals(idea.getId().toString(), ideaResponse.id())
        );
    }

    @SneakyThrows
    @Test
    void getIdeaById() {
        when(ideasRepository.findById(any())).thenReturn(Optional.of(idea));

        var ideaResponse = ideasService.getIdeaById(idea.getId());

        assertAll(
                () -> assertNotNull(ideaResponse),
                () -> assertEquals(idea.getDescription(), ideaResponse.description()),
                () -> assertEquals(idea.getPositiveVotes(), ideaResponse.positiveVotes()),
                () -> assertEquals(idea.getNegativeVotes(), ideaResponse.negativeVotes()),
                () -> assertEquals(idea.getId().toString(), ideaResponse.id())
        );

    }

    @SneakyThrows
    @Test
    void updateIdea() {
        IdeaRequestDto updatedIdeaDto = new IdeaRequestDto("Updated description");

        when(ideasRepository.findById(any())).thenReturn(Optional.of(idea));

        IdeaResponseDto updatedResponse = ideasService.updateIdea(idea.getId(), updatedIdeaDto);

        assertAll(
                () -> assertNotNull(updatedResponse),
                () -> assertEquals(updatedIdeaDto.description(), updatedResponse.description()),
                () -> assertEquals(idea.getPositiveVotes(), updatedResponse.positiveVotes()),
                () -> assertEquals(idea.getNegativeVotes(), updatedResponse.negativeVotes()),
                () -> assertEquals(idea.getId().toString(), updatedResponse.id())
        );
    }

    @Test
    void deleteIdea() {
        when(ideasRepository.findById(any())).thenReturn(Optional.of(idea));

        assertDoesNotThrow(() -> ideasService.deleteIdea(idea.getId()));
    }
}